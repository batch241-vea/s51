import { useState } from 'react';

import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export default function CourseCard({courseProps}) {

  const { name, description, price } = courseProps;

  //Use the state hook for this component to be able to store its state
  //States are used to keep track of information realated to individual components
  /*
    Syntax
    const [getter, setter] = useState(initialGetterValue)
  */

  const [seatState, setSeatState] = useState(30);
  const [count, setCount] = useState(0);

  //console.log(useState);

  function enroll(){
    if (seatState > 0){
      setSeatState(seatState - 1);   
      setCount(count + 1);
      
    } else {
      alert("No more seats");
    } 
  }

  return (
  <Card>
      <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}.</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>
          <Card.Text>Enrollees: {count}</Card.Text>
          {/*<Button variant="primary">Enroll</Button>*/}
          <Button className="bg-primary" onClick={enroll}>Enroll</Button>
      </Card.Body>
  </Card>
  )

}