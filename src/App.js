import './App.css';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';

import { Container } from 'react-bootstrap';

function App() {
  return (
    /*Fragments - common pattern in React.js for a component to return multiple elements*/
    <>
      <AppNavbar/>
      <Container>
        <Home/>
        <Courses/>
      </Container>
    </>
  );
}

export default App;
